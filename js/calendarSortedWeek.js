function getFirstDayOfWeek(date) {
  date.setDate(1); // 设置为该月份的第一天
  return date.getDay(); // 返回星期几，0表示星期日，6表示星期六
}

function reorderDaysOfWeek(firstDayOfWeek) {
  const daysOfWeek = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];

  if (
    firstDayOfWeek !== undefined &&
    firstDayOfWeek >= 0 &&
    firstDayOfWeek < 7
  ) {
    let shiftedArray = daysOfWeek
      .slice(firstDayOfWeek)
      .concat(daysOfWeek.slice(0, firstDayOfWeek));
    daysOfWeek.length = 0; // 清空原数组
    daysOfWeek.push(...shiftedArray); // 将重新排序后的数组赋值给daysOfWeek
  } else {
    console.error("Invalid day of week value.");
  }

  return daysOfWeek;
}

function getFirstOfWeek(year, month, remainSpace) {
  year = year === undefined ? new Date().getFullYear() : year;
  month = month === undefined ? new Date().getMonth() : month - 1;

  let targetDate = new Date(year, month);

  let firstDayOfWeek = getFirstDayOfWeek(targetDate);

  let newArray = reorderDaysOfWeek(firstDayOfWeek);

  if (remainSpace == true) {
    return firstDayOfWeek;
  } else {
    return [...newArray.slice(3, 7), ...newArray.slice(0, 3)].map((s) => s[0]);
  }
}

/*
console.log(getFirstOfWeek(undefined, undefined, true))
console.log(getFirstOfWeek(2024, 7))
*/

function getDays(year, month) {
  year = year ? year : new Date().getFullYear();
  month = month ? month : new Date().getMonth() + 1;
  let totalDays = 31;
  let isLeapYear = (year % 4 === 0 && year % 100 !== 0) || year % 400 == 0;
  if (month == 2 && isLeapYear) {
    totalDays = 29;
  } else if (month == 2 && !isLeapYear) {
    totalDays = 28;
  }
  let day30 = [4, 6, 9, 11];
  for (let d of day30) {
    if (month == d) {
      totalDays = 30;
    }
  }
  return totalDays;
}

/*
console.log(getDays(2023, 2))
console.log(getDays(2024, 8))
console.log(getDays(2024, 2))
*/
